// Login screen toggle

let loginForm = document.querySelector('.login-form-container');

document.querySelector('#login-btn').onclick = () =>{
    loginForm.classList.toggle('active');   
    navbar.classList.remove('active');
}

// Function validade login

function checkLogon() {
    var email = document.getElementById("emailbox").value;
    var password = document.getElementById("passwordbox").value;
    var username = document.getElementById("username").value;
    if (email == "") {
        alert("Please type your email");
        return false;
    }
    if (!email.includes('@')) {
        alert("Please type a valid email");
        return false;
    }
    if (username == "") {
        alert("Please type your Username");
        return false;
    }
    if (password == "") {
        alert("Please type your Password");
        return false;
    }
    if (password.length != 11) {
        alert("Your password must have 11 characters");
        return false;
    }
    alert("Login Successful!! Go to order to start your purchase");
    loginForm.classList.toggle('active');
    return true;
}

// Function add to cart

function addToCart() {
    var burritosOptions = document.getElementById("burritosOptions");
    var burritoOpt = burritosOptions.options[burritosOptions.selectedIndex].value;
    if (burritoOpt == 1) {
        var burritoPrice = 5.0;
    }
    else if (burritoOpt == 2) {
        var burritoPrice = 7.5;
    }
    else if (burritoOpt == 3) {
        var burritoPrice = 17.0;
    }

    // Function coupon and quantity

    var quantity = document.getElementById("quantity").value;
    var quantity = Number(quantity);
    var coupon = document.getElementById("coupon").value;
    if (coupon == "") {
        var totalToPay = quantity * burritoPrice;
    }
    else if (coupon == "mextexgood") {
        var discount = 13.5;
        var total = quantity * burritoPrice;
        var percent = (total / 100) * discount;
        var totalToPay = total - percent;
        alert("Coupon successfully applied!");
    }
    else {
        alert("Coupon not valid, please try again!");
        var totalToPay = quantity * burritoPrice;
    }

    // Quantity number validation
    
    if (isNaN(quantity)) {
        alert("Please insert a valid amount!");
        return false;
    }

    var totalToPay = totalToPay.toLocaleString('de-DE', {
        style: 'currency',
        currency: 'EUR',
        minimumFractionDigits: 2
    });

    document.getElementById("totalToPay").value = totalToPay;
}

// Function health warning

$(document).ready(function () {
    $("#burritosOptions").change(function () {
        var selectedCountry = $("#burritosOptions option:selected").text();
        if (selectedCountry == "SuperSize: €17.00") {
            alert("Attention!!\n The Supersize burrito has 2000 calories per meal")
        }
    });
});

// Function order

function orderNow() {
    var order = document.getElementById("totalToPay").value
    var orderN = Math.floor(1000 + Math.random() * 9000);
    if (order === "" || order === 0) {
        alert("Please add your burrito option to your cart first!")
    }
    else {
        alert("Order number: #" + orderN + ". Your order's in the kitchen!")
    }
}